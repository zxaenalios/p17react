import React, { useState, useEffect } from 'react'

function DateFunction(props) {
    const [date, setDate] = useState(new Date())

    useEffect(() => {
        const timer = setInterval(() => {
            setDate(new Date())
        }, 1000);
        return () => {
            clearInterval(timer)
        }
    }, [])

    return (
        <>
            <h1>Date Function</h1>
            <p>{date.toLocaleDateString()}</p>
        </>
    )
}
export default DateFunction