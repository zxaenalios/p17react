import React, { useState } from "react";
import { Button, FormGroup, FormControl, FormLabel, NavLink } from "react-bootstrap";
import './Login.css';
import { useHistory } from "react-router-dom";


function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const history = useHistory();

  function validateForm() {
    return email.length > 0 && password.length > 7;
  }

  function onSubmit(e) {
    e.preventDefault();
    if (email == "zxaenalios@gmail.com" && password == "12345678") { 
    history.push("/home");
    }
  }

  return (
    <div className="Login">
      <h1 className="text-center">Halaman Login</h1>
      <form onSubmit={onSubmit}>
        <FormGroup controlId="email">
          <FormLabel>Email</FormLabel>
          <FormControl
            autoFocus
            type="email"
            value={email}
            onChange={e => setEmail(e.target.value)}
          />
        </FormGroup>
        <FormGroup controlId="password">
          <FormLabel>Password</FormLabel>
          <FormControl
            value={password}
            onChange={e => setPassword(e.target.value)}
            type="password"
          />
        </FormGroup>
        <Button type="submit">
          Login
        </Button>
        
      </form>
    </div>
  );
}

export default Login