import React from 'react'
import {BrowserRouter, Route} from 'react-router-dom'
import Login from './Login'
import Home from './Home'
import About from './About'
import Contact from './Contact'

function Router(){
    return (
        <BrowserRouter>
           
            <Route exact path="/" component={Login}/>
            <Route exact path="/home" component={Home}/>
            <Route exact path="/about" component={About}/>
            <Route exact path="/contact" component={Contact}/>

        </BrowserRouter>
    )
}

export default Router