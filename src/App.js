import React from 'react';
import logo from './logo.svg';
import './App.css';
import Clock from './Clock';
import Timer from './components/Timer';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        {/* Untuk Aplikasi Clock */}
        {/* <Clock/> */}
        {/* Untuk Aplikasi Timer */}
        <Timer/>
      </header>
    </div>
  );
}

export default App;
