import React, { Fragment } from 'react'

class DateClass extends React.Component {
    render() {
        return (
            <Fragment>
                <h1>Date Class</h1>
                <p>{this.props.date.toDateString()}</p>
            </Fragment>
        )
    }
}
export default DateClass