import React from 'react'

// Form Input Timer
class TimerInput extends React.Component {
    render() {
      return (
        <React.Fragment>
          <h3>Input Timer</h3>
          <input type="number" value={this.props.timer} onInput={this.props.onInput} required />
        </React.Fragment>
      );
    }
  }
// Show Timer
const Time = function(props) {
	return (
		<h1>
			{props.time}
		</h1>
	);
};

// Reset Timer
const Reset = function(props) {
	return (
		<button type="button" onClick={props.onClickReset} className="btn btn-secondary reset">
			Reset
		</button>
	);
};

// Start/Stop button
class Control extends React.Component {
	constructor(props) {
		super(props);
	};  
    onClickHandler = () => {
        if(this.props.paused){
        this.props.start();
        }
        else{
        this.props.stop();
        }
    }
	render() {
		return (
                <button type="button" className={this.props.paused?"btn btn-primary Stop":"btn btn-danger"} 
                onClick={this.onClickHandler}>
		    	{this.props.paused?"Start":"Stop"}
		    </button>
		);
	};
};


class Timer extends React.Component {
	constructor(props) {
		super(props);
        this.state = { timer: '', paused: true };
        this.onInput = this.onInput.bind(this);
    };

    onInput(event) {
        this.setState({
          timer: event.target.value
        });
    }
  
    tick = () => {
        const timer = this.state.timer
        if (timer==0) {
            this.setState({ paused : true });
        }
        else {
        this.setState({ timer : this.state.timer - 1 });}
    }
  
	startTimer = () =>{
		this.interval = setInterval(this.tick,1000);
        this.setState({ paused : false });
	}
  
    stopTimer = () => {
        clearInterval( this.interval );
        this.setState({ paused : true });
        }
    
    reset = () => {
        this.setState({ timer : 0, paused: true });
        clearInterval( this.interval );
    }

	render() {
        const timer = this.state.timer
		return (
			<div>
                <TimerInput value={this.state.timer} 
                onInput={this.onInput} />
              
                
                { timer == 0
                ? <h1>0</h1>
                : <Time time={this.state.timer}/>
                }
                
                <Control 
                paused={this.state.paused} 
                start={this.startTimer} 
                stop={this.stopTimer} 
                />
                <Reset  onClickReset={this.reset}/>
            </div>
		);
	};
};

export default Timer