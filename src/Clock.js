import React from 'react'
import DateClass from './DateClass'
import DateFunction from './DateFunction'

// class Clock extends React.Component {
//     constructor() {
//         super()
//         this.state = {
//             waktu: new Date()
//         }
//     }

//     tick() {
//         this.setState({
//             waktu: new Date()
//         })
//     }
//     componentDidMount() {
//         this.timer = setInterval(
//             () => this.tick(), 1000
//         )
//     }
//     componentWillUnmount() {
//         clearInterval(this.timer)
//     }
//     render() {
//         return (
//             <div>
//                 <h2>{this.state.waktu.toLocaleTimeString()}</h2>
//             </div>
//         )
//     }
// }

class Clock extends React.Component {
    constructor() {
        super()
        this.state = {
            dateTime: new Date(),
            dateIsFunction: false
        }
        this.handleClick = this.handleClick.bind(this)
    }
    handleClick() {
        this.setState(state => ({
            dateIsFunction: !state.dateisFunction
        })); 
    }
    render() {
        var dateComponent;
        if (this.state.dateIsFunction){
            dateComponent = <DateFunction/>
        } else {
            dateComponent = <DateClass date={this.state.dateTime}/>
        }
        return (
            <div>
                {dateComponent}
                {this.state.dateTime.toLocaleTimeString()}
                <br></br>
                <p>Change date component to</p>
                <button onClick={this.handleClick} style={{padding: '5px 10px', fontSize: '1rem'}}>
                    {this.state.dateIsFunction ? 'CLASS' : 'FUNCTION'}
                </button>
            </div>
        )
    }
}
export default Clock